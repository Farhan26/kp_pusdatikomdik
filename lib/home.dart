import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:imagebutton/imagebutton.dart';
import 'package:radio_disdik/widget/button.dart';
import 'package:radio_disdik/widget/widget.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              height: 850,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('lib/assets/images/back2.png'),
                    fit: BoxFit.cover,
                  )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 36,
                      ),
                      //header
                      Row(
                        children: [
                          //logo jaya raya
                          Container(
                            margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            width: 50,
                            child: new Image.asset('lib/assets/images/jaya raya.png'),
                          ),
                          //tulisan
                          Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 266,
                                    child: Text('PEMERINTAH PROVINSI DKI JAKARTA DINAS PENDIDIKAN',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                                    ),
                                  )
                                ],
                              )

                          ),
                        ],
                      ),

                      SizedBox(
                        height: 8,
                      ),

                      Container(
                        width: 200,
                        child: new Image.asset('lib/assets/images/logo2.png'),
                      ),

                      //tulisan aplikasi
                      Container(
                        child: Text('APLIKASI',
                          style: TextStyle(
                              fontSize: 64,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              shadows: [Shadow(
                                  offset: Offset(-3, -3),
                                  color: Colors.black
                              )]
                          ),
                        ),
                      ),

                      SizedBox(
                        height: 16,
                      ),
                      //button
                      Container(
                        child: Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Buttons(
                                      text: 'DISDIK',
                                      link: () => {
                                        launch(
                                            'https://disdik.jakarta.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'K J P',
                                      link: () => {
                                        launch(
                                            'https://kjp.jakarta.go.id/kjp2/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'O C S',
                                      link: () =>
                                      {launch('http://103.134.18.34/ocs/')},
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Buttons(
                                      text: 'radioDISDIK',
                                      link: () =>
                                      {launch('https://pudatikomdik.id/')},
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'eRKAS',
                                      link: () =>
                                      {launch('https://rkas.jakarta.go.id/')},
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'eBKU',
                                      link: () =>
                                      {launch('https://siap.jakarta.go.id/')},
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Buttons(
                                      text: 'Sidado',
                                      link: () => {
                                        launch(
                                            'https://sidado.jakarta.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'Sidanira',
                                      link: () => {
                                        launch(
                                            'https://sidanira.jakarta.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'BERITA',
                                      link: () =>
                                      {launch('https://jakarta.go.id/')},
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Buttons(
                                        text: 'PPDB',
                                        link: () =>
                                        {launch('https://siap-ppdb.com')},
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 0, 8, 0)),
                                    Buttons(
                                        text: 'Home Learning',
                                        link: () =>
                                        {launch('http://27.124.92.60/')},
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 0, 8, 0)),
                                    Buttons(
                                        text: 'eKP',
                                        link: () =>
                                        {launch('http://ekp.jakarta.go.id/')},
                                        margin: const EdgeInsets.fromLTRB(
                                            0, 0, 0, 0)),
                                  ]),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: SizedBox(
                                width: 135,
                                height: 31,
                                child: ElevatedButton(
                                    onPressed: () => {
                                                launch(
                                                    'https://e-bpms.jakarta.go.id/main')
                                              },
                                    style: ElevatedButton.styleFrom(
                                        primary: Color.fromRGBO(224, 224, 224, 1.0),
                                        onPrimary: Color.fromRGBO(224, 224, 224, 1.0),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: new BorderRadius.circular(30.0),
                                        )
                                    ),
                                    child: Text('SISPENDAPODIK', textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12
                                      ),
                                    )
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 16,
                      ),

                      Container(
                        height: 10.0,
                        width: 320.0,
                        // color: Colors.transparent,
                        child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [Color(0xfff5f3ae), Color(0xff504f31)]),
                              borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                        ),
                      ),

                      SizedBox(
                        height: 16,
                      ),

                      Container(
                        child: Text(
                          'Link Eksternal',
                          style: TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              shadows: [
                                Shadow(
                                    offset: Offset(-3, -3), color: Colors.black)
                              ]),
                        ),
                      ),

                      SizedBox(
                        height: 16,
                      ),

                      //button
                      Container(
                        child: Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                children: [
                                  Buttons(
                                      text: 'DAPODIK',
                                      link: () => {
                                        launch(
                                            'https://dapo.kemdikbud.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'VERVAL PD',
                                      link: () => {
                                        launch(
                                            'https://vervalpd.data.kemdikbud.go.id')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'VERVAL PTK',
                                      link: () => {
                                        launch(
                                            'http://vervalptk.data.kemdikbud.go.id')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                              child: Row(
                                children: [
                                  Buttons(
                                      text: 'CEK NISN',
                                      link: () => {
                                        launch(
                                            'https://nisn.data.kemdikbud.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'INFO GTK',
                                      link: () => {
                                        launch(
                                            'https://info.gtk.kemdikbud.go.id')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 8, 0)),
                                  Buttons(
                                      text: 'Rumah Belajar',
                                      link: () => {
                                        launch(
                                            'https://belajar.kemdikbud.go.id/')
                                      },
                                      margin:
                                      const EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      Expanded(
                        child: Align(
                          alignment: FractionalOffset.bottomCenter,

                          child: MaterialButton(
                            onPressed: () => {},
                            child: Container(
                              // margin: const EdgeInsets.fromLTRB(0, 64, 0, 0),
                              child: Row(
                                children: [
                                  ImageButton(
                                    children: <Widget>[],
                                    width: 48,
                                    height: 48,
                                    paddingTop: 5,
                                    pressedImage: Image.asset(
                                      "lib/assets/images/Facebook.png",
                                    ),
                                    unpressedImage: Image.asset("lib/assets/images/Facebook.png"),
                                    onTap: () => { launch('https://www.facebook.com/pusdatikomdik')},
                                  ),

                                  SizedBox(
                                    width: 25,
                                  ),

                                  ImageButton(
                                    children: <Widget>[],
                                    width: 48,
                                    height: 48,
                                    paddingTop: 5,
                                    pressedImage: Image.asset(
                                      "lib/assets/images/whatsapp.png",
                                    ),
                                    unpressedImage: Image.asset("lib/assets/images/whatsapp.png"),
                                    onTap: () => { launch("https://linktr.ee/pusdatikomdik")},
                                  ),

                                  SizedBox(
                                    width: 25,
                                  ),

                                  ImageButton(
                                    children: <Widget>[],
                                    width: 48,
                                    height: 48,
                                    paddingTop: 5,
                                    pressedImage: Image.asset(
                                      "lib/assets/images/Instagram.png",
                                    ),
                                    unpressedImage: Image.asset("lib/assets/images/Instagram.png"),
                                    onTap: () => { launch("https://www.instagram.com/radiodisdik/")},
                                  ),

                                  SizedBox(
                                    width: 25,
                                  ),

                                  ImageButton(
                                    children: <Widget>[],
                                    width: 48,
                                    height: 48,
                                    paddingTop: 5,
                                    pressedImage: Image.asset(
                                      "lib/assets/images/YouTube.png",
                                    ),
                                    unpressedImage: Image.asset("lib/assets/images/YouTube.png"),
                                    onTap: () => { launch("https://www.youtube.com/channel/UCpJMrFXpylOMjZwPK5qGPvg")},
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: 12,
                      )

                    ],
                  ),
                ],
              ),
            ),
          ),
        ),

      )
    );
    // throw UnimplementedError();
  }
}