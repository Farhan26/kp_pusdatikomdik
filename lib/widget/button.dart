import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Buttons extends StatelessWidget {

  final String text;
  final Function link;
  final EdgeInsetsGeometry margin;

  const Buttons({Key key,
    @required this.text,
    @required this.link,
    @required this.margin
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: SizedBox(
        width: 115,
        height: 31,
        child: ElevatedButton(
            onPressed: link,
            style: ElevatedButton.styleFrom(
                primary: Color.fromRGBO(224, 224, 224, 1.0),
                onPrimary: Color.fromRGBO(224, 224, 224, 1.0),
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                )
            ),
            child: Text(text, textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            )
        ),
      ),
    );
  }
}